package dao;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.DatatypeConverter;

import model.User;



public class UserDao {
	//ログインID、パスワードの一致するユーザーを探す
	public User findByLoginInfo(String loginId,String password) {
		Connection conn = null;

		//ハッシュを生成したい元の文字列
		String source = password;
		//ハッシュ生成前にバイト配列に置き換える際のCharset
		Charset charset = StandardCharsets.UTF_8;
		//ハッシュアルゴリズム
		String algorithm = "MD5";

		//ハッシュ生成処理
		byte[] bytes = null;;
		try {
			bytes = MessageDigest.getInstance(algorithm).digest(source.getBytes(charset));
		} catch (NoSuchAlgorithmException e1) {
			// TODO 自動生成された catch ブロック
			e1.printStackTrace();
		}
		String result = DatatypeConverter.printHexBinary(bytes);

        try {
            // データベースへ接続
        	conn = DBManager.getConnection();

        	//select文
        	String sql = "select * from user where login_id = ? and password = ?";

        	//select文を実行し、結果を取得
        	PreparedStatement pstmt = conn.prepareStatement(sql);
        	pstmt.setString(1, loginId);
        	pstmt.setString(2, result);
        	ResultSet rs = pstmt.executeQuery();

        	//ログイン失敗の場合(0件の時)：null返す
        	if (!rs.next()) {
        		return null;
        	}

        	//ログイン成功の場合(1件の時)：データをBeansインスタンスのフィールドにセットして返す
        	int idData = rs.getInt("id");
        	String loginidData = rs.getString("login_id");
        	String nameData = rs.getString("name");
        	User user = new User(idData,loginidData,nameData);
        	return user;

        } catch (SQLException e) {
        	e.printStackTrace();
        	return null;
        } finally {
	        // データベース切断
	        if (conn != null) {
	          	try {
	                conn.close();
	            } catch (SQLException e) {
	                e.printStackTrace();
	                return null;
	            }
	        }
        }
	}


	//管理者以外の全ユーザ情報を取得
	public List<User> findAll() {
		Connection conn = null;
        List<User> userList = new ArrayList<User>();

        try {
            // データベースへ接続
        	conn = DBManager.getConnection();

        	//select文
        	String sql = "select * from user where login_id != 'admin'";

        	//select文を実行し、結果を取得
        	PreparedStatement pstmt = conn.prepareStatement(sql);
            ResultSet rs = pstmt.executeQuery();

            //Listに追加
            while(rs.next()) {
            	int id = rs.getInt("id");
            	String loginId = rs.getString("login_id");
            	String name = rs.getString("name");
            	Date birthDate = rs.getDate("birth_date");
            	String password = rs.getString("password");
            	String createDate = rs.getString("create_date");
            	String updateDate = rs.getString("update_date");

            	User user = new User(id, loginId, name, birthDate, password, createDate, updateDate);
            	userList.add(user);
            }
        } catch (SQLException e) {
        	e.printStackTrace();
        	return null;
        } finally {
	        // データベース切断
	        if (conn != null) {
	          	try {
	                conn.close();
	            } catch (SQLException e) {
	                e.printStackTrace();
	                return null;
	            }
	        }
        }
        return userList;
	}

	//検索ワードに一致するユーザ情報を取得
	public List<User> searchUser(String sLoginId, String sName, String fromDate, String toDate) {
		Connection conn = null;
        List<User> userList = new ArrayList<User>();

        try {
            // データベースへ接続
        	conn = DBManager.getConnection();

        	//select文
        	String sql = "select * from user where login_id != 'admin'";

        	if(!(sLoginId.isEmpty())) {
        		sql += " and login_id = '"+ sLoginId + "'";
        	}

        	if(!(sName.isEmpty())) {
        		sql += "and name like '%" + sName + "%'";
        	}

        	if(!(fromDate.isEmpty())) {
        		sql += "and birth_date >= '" + fromDate + "'";
        	}

        	if(!(toDate.isEmpty())) {
        		sql += "and birth_date <= '" + toDate +"'";
        	}

        	//System.out.println(sql);

        	//select文を実行し、結果を取得
        	Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            //Listに追加
            while(rs.next()) {
            	int id = rs.getInt("id");
            	String loginId = rs.getString("login_id");
            	String name = rs.getString("name");
            	Date birthDate = rs.getDate("birth_date");
            	String password = rs.getString("password");
            	String createDate = rs.getString("create_date");
            	String updateDate = rs.getString("update_date");

            	User user = new User(id, loginId, name, birthDate, password, createDate, updateDate);
            	userList.add(user);
            }
        } catch (SQLException e) {
        	e.printStackTrace();
        	return null;
        } finally {
	        // データベース切断
	        if (conn != null) {
	          	try {
	                conn.close();
	            } catch (SQLException e) {
	                e.printStackTrace();
	                return null;
	            }
	        }
        }
        return userList;
	}

	//ログインIDが使用済か確認
	public Boolean idCheck(String loginId) {
		Connection conn = null;
	    try {
	        // データベースへ接続
	    	conn = DBManager.getConnection();

	    	//select文
	    	String sql = "select * from user where login_id = ?";

	    	//select文を実行し、結果を取得
	    	PreparedStatement pstmt = conn.prepareStatement(sql);
	    	pstmt.setString(1, loginId);
	    	ResultSet rs = pstmt.executeQuery();

	    	//ログインIDが使用済の場合(1件の時)：
	    	if (rs.next()) {
	    		return false;
	    	}

	    	//ログインIDが未使用の場合(0件の時)：
	    	return true;

	    } catch (SQLException e) {
	    	e.printStackTrace();
	    	return null;
	    } finally {
	        // データベース切断
	        if (conn != null) {
	          	try {
	                conn.close();
	            } catch (SQLException e) {
	                e.printStackTrace();
	                return null;
	            }
	        }
	    }
	}


	//新規登録
	public void userRegister(String loginId, String password, String name, String birthDate) {

		//ハッシュを生成したい元の文字列
		String source = password;
		//ハッシュ生成前にバイト配列に置き換える際のCharset
		Charset charset = StandardCharsets.UTF_8;
		//ハッシュアルゴリズム
		String algorithm = "MD5";

		//ハッシュ生成処理
		byte[] bytes = null;;
		try {
			bytes = MessageDigest.getInstance(algorithm).digest(source.getBytes(charset));
		} catch (NoSuchAlgorithmException e1) {
			// TODO 自動生成された catch ブロック
			e1.printStackTrace();
		}
		String result = DatatypeConverter.printHexBinary(bytes);

		Connection conn = null;

		try {
            // データベースへ接続
        	conn = DBManager.getConnection();

        	//insert文
        	String sql = "insert into user (login_id, password, name, birth_date, create_date, update_date) values(?,?,?,?,now(),now())";

        	//insert文を実行し、結果を取得
        	PreparedStatement pstmt = conn.prepareStatement(sql);
        	pstmt.setString(1, loginId);
        	pstmt.setString(2, result);
        	pstmt.setString(3, name);
        	pstmt.setString(4, birthDate);

        	pstmt.executeUpdate();


        } catch (SQLException e) {
        	e.printStackTrace();
        } finally {
	        // データベース切断
	        if (conn != null) {
	          	try {
	                conn.close();
	            } catch (SQLException e) {
	                e.printStackTrace();
	            }
	        }
        }
	}

	//ユーザ詳細取得
	public User userDetail(String id) {
		Connection conn = null;

        try {
            // データベースへ接続
        	conn = DBManager.getConnection();

        	//select文
        	String sql = "select * from user where id = ?";

        	//select文を実行し、結果を取得
        	PreparedStatement pstmt = conn.prepareStatement(sql);
	    	pstmt.setString(1, id);
            ResultSet rs = pstmt.executeQuery();

          	//：null返す
        	if (!rs.next()) {
        		return null;
        	}

            //idの詳細情報を返す
        	String loginId = rs.getString("login_id");
        	String name = rs.getString("name");
        	Date birthDate = rs.getDate("birth_date");
        	String password = rs.getString("password");
        	String createDate = rs.getString("create_date");
        	String updateDate = rs.getString("update_date");

            User user = new User(Integer.parseInt(id), loginId, name, birthDate, password, createDate, updateDate);
            return user;

        } catch (SQLException e) {
        	e.printStackTrace();
        	return null;
        } finally {
	        // データベース切断
	        if (conn != null) {
	          	try {
	                conn.close();
	            } catch (SQLException e) {
	                e.printStackTrace();
	                return null;
	            }
	        }
        }
	}

	//ユーザ情報更新
	public void userUpdate(String id, String password, String name, String birthDate) {

		Connection conn = null;

        try {
            // データベースへ接続
        	conn = DBManager.getConnection();

        	//update
        	String sql = "update user set update_date = now(), name = '" + name + "', birth_date = '" + birthDate + "'";

        	if(!(password.isEmpty())) {
        		//ハッシュを生成したい元の文字列
    			String source = password;
    			//ハッシュ生成前にバイト配列に置き換える際のCharset
    			Charset charset = StandardCharsets.UTF_8;
    			//ハッシュアルゴリズム
    			String algorithm = "MD5";

    			//ハッシュ生成処理
    			byte[] bytes = null;;
    			try {
    				bytes = MessageDigest.getInstance(algorithm).digest(source.getBytes(charset));
    			} catch (NoSuchAlgorithmException e1) {
    				// TODO 自動生成された catch ブロック
    				e1.printStackTrace();
    			}
    			String result = DatatypeConverter.printHexBinary(bytes);

        		sql += ",password = '" + result + "'";
        	}

        	if(!(id.isEmpty())) {
        		sql += "where id ='"+ id + "'";
        	}

        	//実行
        	Statement stmt = conn.createStatement();
        	stmt.executeUpdate(sql);

        } catch (SQLException e) {
        	e.printStackTrace();
        } finally {
	        // データベース切断
	        if (conn != null) {
	          	try {
	                conn.close();
	            } catch (SQLException e) {
	                e.printStackTrace();
	            }
	        }
        }
	}
	//登録削除
	public void userDelete(String id) {
		Connection conn = null;

		try {
            // データベースへ接続
        	conn = DBManager.getConnection();

        	//delete文
        	String sql = "delete from user where id = ?";

        	//delete文を実行し、結果を取得
        	PreparedStatement pstmt = conn.prepareStatement(sql);
        	pstmt.setString(1, id);

        	pstmt.executeUpdate();

        } catch (SQLException e) {
        	e.printStackTrace();
        } finally {
	        // データベース切断
	        if (conn != null) {
	          	try {
	                conn.close();
	            } catch (SQLException e) {
	                e.printStackTrace();
	            }
	        }
        }


	}
}
