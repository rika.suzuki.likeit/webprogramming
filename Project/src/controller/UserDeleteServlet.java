package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserDao;
import model.User;

/**
 * Servlet implementation class UserDeleteServlet
 */
@WebServlet("/UserDeleteServlet")
public class UserDeleteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserDeleteServlet() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//ログインセッションがない場合：ログイン画面へリダイレクト
        HttpSession session = request.getSession();
		User loginCheck = (User)session.getAttribute("userInfo");

		if(loginCheck == null) {
			response.sendRedirect("LoginServlet");
		}else {
			//URLからgetパラメータとしてIDを受け取る
			String id = request.getParameter("id");

			//idのユーザの情報を取得
			UserDao userDao = new UserDao();
			User user = userDao.userDetail(id);

			//取得した詳細情報をリクエストスコープに保存
			request.setAttribute("user", user);

			//削除画面jspへフォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userDelete.jsp");
			dispatcher.forward(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//URLからgetパラメータとしてID受け取る
		String id = request.getParameter("id");

		//idのユーザ情報を削除
		UserDao userDao = new UserDao();
		userDao.userDelete(id);

		//ユーザ一覧画面へリダイレクト
		response.sendRedirect("UserListServlet");

	}

}
