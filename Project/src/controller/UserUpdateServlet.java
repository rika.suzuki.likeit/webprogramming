package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserDao;
import model.User;

/**
 * Servlet implementation class UserUpdateServlet
 */
@WebServlet("/UserUpdateServlet")
public class UserUpdateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserUpdateServlet() {

    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//ログインセッションがない場合：ログイン画面へリダイレクト
        HttpSession session = request.getSession();
		User loginCheck = (User)session.getAttribute("userInfo");

		if(loginCheck == null) {
			response.sendRedirect("LoginServlet");
		}else {
			//URLからgetパラメータとしてIDを受け取る
			String id = request.getParameter("id");

			//idを引数として、そのユーザの情報を取得
			UserDao userDao = new UserDao();
			User user = userDao.userDetail(id);

			//取得した詳細情報をリクエストスコープに保存
			request.setAttribute("user", user);

			//更新画面jspへフォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userUpdate.jsp");
			dispatcher.forward(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// リクエストパラメータの文字コードを指定
        request.setCharacterEncoding("UTF-8");

        //URLからgetパラメータとしてIDを受け取る
		String id = request.getParameter("id");
		String loginId = request.getParameter("login_id");

        //リクエストパラメータを取得
        String password = request.getParameter("password");
        String password2 = request.getParameter("password2");
        String name = request.getParameter("name");
        String birthDate = request.getParameter("birthDate");

        //更新可否フラグ：否ならfalse
	    Boolean flag = true;

        if(password.isEmpty() && password2.isEmpty()) {
        	if(name.isEmpty() || birthDate.isEmpty()) {
        		flag = false;
        	}
        }else
		//パスワードが不一致の場合
		if (!password.equals(password2)) {
			flag = false;
	    //未入力項目がある場合
		}else if(password.isEmpty() || password2.isEmpty() || name.isEmpty() || birthDate.isEmpty()) {
			flag = false;
		}

	    //更新不可の場合：更新画面へ戻る、パスワード以外の部分は引き継ぎ表示する
		if (flag == false) {
			//リクエストスコープにエラーメッセージをセット
	    	request.setAttribute("errMsg", "入力された内容は正しくありません。");

	    	//入力されたユーザ名、生年月日をリクエストスコープにセット
	    	User user = new User(Integer.parseInt(id),loginId,name,birthDate);
	       	request.setAttribute("user",user);

	    	//ログイン画面にフォワード
	    	RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userUpdate.jsp");
	    	dispatcher.forward(request, response);

		}

        //更新成功の場合：ユーザ一覧画面へリダイレクト
        //リクエストパラメータを引数に渡してdaoを実行
        UserDao userDao = new UserDao();
       	userDao.userUpdate(id, password, name, birthDate);

        //更新された情報でログインセッションを保存
        User user = userDao.userDetail(id);
        HttpSession session = request.getSession();
        session.setAttribute("userInfo", user);

        //ユーザ一覧画面へリダイレクト
        response.sendRedirect("UserListServlet");
	}

}
