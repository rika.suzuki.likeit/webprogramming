package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserDao;
import model.User;

/**
 * Servlet implementation class UserListServlet
 */
@WebServlet("/UserListServlet")
public class UserListServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserListServlet() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//ログインセッションがない場合：ログイン画面へリダイレクト
        HttpSession session = request.getSession();
		User loginCheck = (User)session.getAttribute("userInfo");

		if(loginCheck == null) {
			response.sendRedirect("LoginServlet");
		}else {

		//全ユーザ情報を取得
		UserDao userDao = new UserDao();
		List<User> userList = userDao.findAll();

		//リクエストスコープにユーザ一覧情報をセット
		request.setAttribute("userList", userList);

		//ユーザ一覧画面へフォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userList.jsp");
		dispatcher.forward(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

	//ユーザ検索機能

    // リクエストパラメータの文字コードを指定
    request.setCharacterEncoding("UTF-8");

	//リクエストパラメータを取得
	String loginId = request.getParameter("loginId");
	String name = request.getParameter("name");
	String birthDate1 = request.getParameter("birthDate1");
	String birthDate2 = request.getParameter("birthDate2");

	//条件でユーザ検索
	UserDao userDao = new UserDao();
	List<User> userList = userDao.searchUser(loginId, name, birthDate1, birthDate2);

	//リクエストスコープにユーザ一覧情報をセット
	request.setAttribute("userList", userList);

	//ユーザ一覧画面へフォワード
	RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userList.jsp");
	dispatcher.forward(request, response);
	}





}
