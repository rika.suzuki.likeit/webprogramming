package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserDao;
import model.User;

/**
 * Servlet implementation class UserRegisterServlet
 */
@WebServlet("/UserRegisterServlet")
public class UserRegisterServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserRegisterServlet() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//ログインセッションがない場合：ログイン画面へリダイレクト
        HttpSession session = request.getSession();
		User loginCheck = (User)session.getAttribute("userInfo");

		if(loginCheck == null) {
			response.sendRedirect("LoginServlet");
		}else {
			//新規登録jspへフォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userRegister.jsp");
			dispatcher.forward(request, response);
		}
	}


	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // リクエストパラメータの文字コードを指定
        request.setCharacterEncoding("UTF-8");

        //リクエストパラメータを取得
        String loginId = request.getParameter("loginId");
        String password = request.getParameter("password");
        String password2 = request.getParameter("password2");
        String name = request.getParameter("name");
        String birthDate = request.getParameter("birthDate");

        //登録可否フラグ：否ならfalse
        Boolean flag = true;

    	//パスワードが不一致の場合
    	if (!password.equals(password2)) {
    		flag = false;
        //未入力項目がある場合
    	}else if(loginId.isEmpty() || password.isEmpty() || password2.isEmpty() || name.isEmpty() || birthDate.isEmpty()) {
    		flag = false;
    	}else {
    	//ログインIDが使用済の場合
	    	UserDao IdCheck = new UserDao();
	    	flag = IdCheck.idCheck(loginId);
    	}

        //登録不可の場合：新規登録画面へ戻る、パスワード以外の部分は引き継ぎ表示する
    	if (flag == false) {
    		//リクエストスコープにエラーメッセージをセット
        	request.setAttribute("errMsg", "入力された内容は正しくありません。");

        	//入力されたログインID、ユーザ名、生年月日をリクエストスコープにセット
        	request.setAttribute("loginId", loginId);
        	request.setAttribute("name", name);
        	request.setAttribute("birthDate", birthDate);

        	//ログイン画面にフォワード
        	RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userRegister.jsp");
        	dispatcher.forward(request, response);
        	return;

    	}

        //登録成功の場合：ユーザ一覧画面へリダイレクト
        //リクエストパラメータを引数に渡してdaoを実行
        UserDao userDao = new UserDao();
        userDao.userRegister(loginId, password, name, birthDate);

        response.sendRedirect("UserListServlet");

	}

}
