package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserDao;
import model.User;

/**
 * Servlet implementation class LoginServlet
 */
@WebServlet("/LoginServlet")
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public LoginServlet() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		//ログインセッションがある場合：ユーザ一覧へリダイレクト
        HttpSession session = request.getSession();
		User loginCheck = (User)session.getAttribute("userInfo");

		if(loginCheck != null) {
			response.sendRedirect("UserListServlet");
		}else {
			//ログイン画面へフォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/login.jsp");
			dispatcher.forward(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // リクエストパラメータの文字コードを指定
        request.setCharacterEncoding("UTF-8");

        //リクエストパラメータを取得
        String loginId = request.getParameter("loginId");
        String password = request.getParameter("password");

        //リクエストパラメータを引数に渡して、daoを実行
        UserDao userDao = new UserDao();
        User user = userDao.findByLoginInfo(loginId,password);

        //ログイン失敗の場合：ログイン画面へフォワード
        if(user == null) {
        	//リクエストスコープにエラーメッセージをセット
        	request.setAttribute("errMsg", "ログインIDまたはパスワードが異なります。");

        	//ログイン画面にフォワード
        	RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/login.jsp");
        	dispatcher.forward(request, response);
        	return;
        }

        //ログイン成功の場合：ユーザ一覧画面へリダイレクト
        //ユーザ情報をセッションスコープにセット
        HttpSession session = request.getSession();
        session.setAttribute("userInfo", user);

        //リダイレクト
        response.sendRedirect("UserListServlet");
	}

}
