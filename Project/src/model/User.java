package model;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class User implements Serializable{
	private int id;
	private String login_id;
	private String name;
	private Date birthDate;
	private String password;
	private String createDate;
	private String updateDate;

	//ログインセッション保存用のコンストラクタ
	public User(int id, String loginId,String name){
		this.id = id;
		this.login_id = loginId;
		this.name = name;
	}

	//全データ保存用のコンストラクタ
	public User(int id, String loginId,String name, Date birthDate, String password, String createDate, String updateDate){
		this.id = id;
		this.login_id = loginId;
		this.name = name;
		this.birthDate = birthDate;
		this.password = password;
		this.createDate = createDate;
		this.updateDate = updateDate;
	}

	//更新用のコンストラクタ
	public User(int id, String loginId,String name, String birthDate){
		this.id = id;
		this.login_id = loginId;
		this.name = name;
		setBirthDate(birthDate);
	}



	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getLogin_id() {
		return login_id;
	}
	public void setLogin_id(String login_id) {
		this.login_id = login_id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Date getBirthDate() {
		return birthDate;
	}

	public String getBirthDateStr() {
		 String str = new SimpleDateFormat("yyyy-MM-dd").format(this.birthDate);
		return str;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	public void setBirthDate(String birthDate) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

        // Date型変換
        Date formatDate = null;
		try {
			formatDate = sdf.parse(birthDate);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		this.birthDate = formatDate;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	public String getCreateDate() {
		return createDate;
	}
	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}
	public String getUpdateDate() {
		return updateDate;
	}
	public void setUpdateDate(String updateDate) {
		this.updateDate = updateDate;
	}

}
