<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html lang="ja">
<head>
	<meta charset="UTF-8">
	<title>ユーザ情報詳細</title>
	<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
</head>
<body>

	<header>
		<nav class="navbar navbar-expand navbar-secondary bg-secondary">
		<div class="collapse navbar-collapse justify-content-end">
			<ul class="navbar-nav">
				<li class="nav-text">${userInfo.name}さん</li>
				<li class="nav-item">
					<a class="nav-link" href="LogoutServlet">ログアウト</a>
				</li>
			</ul>
		</div>
		</nav>
	</header>
	<br>
	<div class="container">

		<h1 class="text-center">ユーザ情報詳細参照</h1>

			<div class="form-horizontal">

				<div class="form-group row">
					<label class="col-sm-2 col-form-label">ログインID</label>
					<div class="col-sm-10">
						<p class="form-control-plaintext">${user.login_id}</p>
					</div>
				</div>

				<div class="form-group row">
					<label class="col-sm-2 col-form-label">ユーザ名</label>
					<div class="col-sm-10">
						<p class="form-control-plaintext">${user.name}</p>
					</div>
				</div>

				<div class="form-group row">
					<label class="col-sm-2 col-form-label">生年月日</label>
					<div class="col-sm-10">
						<p class="form-control-plaintext">${user.birthDate}</p>
					</div>
				</div>

				<div class="form-group row">
					<label class="col-sm-2 col-form-label">登録日時</label>
					<div class="col-sm-10">
						<p class="form-control-plaintext">${user.createDate}</p>
					</div>
				</div>

				<div class="form-group row">
					<label class="col-sm-2 col-form-label">更新日時</label>
					<div class="col-sm-10">
						<p class="form-control-plaintext">${user.updateDate}</p>
					</div>
				</div>

				<a href="UserListServlet">戻る</a>

			</div>

	</div>
</body>
</html>