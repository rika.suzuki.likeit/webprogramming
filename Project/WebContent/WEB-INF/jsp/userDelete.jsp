<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html lang="ja">
<head>
	<meta charset="UTF-8">
	<title>ユーザ削除</title>
	<link rel="stylesheet"
		href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
		integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
		crossorigin="anonymous">
</head>
<body>

	<header>
		<nav class="navbar navbar-expand navbar-secondary bg-secondary">
		<div class="collapse navbar-collapse justify-content-end">
			<ul class="navbar-nav">
				<li class="nav-text">${userInfo.name}さん</li>
				<li class="nav-item">
					<a class="nav-link" href="LogoutServlet">ログアウト</a>
				</li>
			</ul>
		</div>
		</nav>
	</header>
	<br>
	<div class="container">

		<h1 class="text-center">ユーザ削除確認</h1>

			<p>ログインID：${user.login_id} を削除しますか？</p>
			<br>
			<div class="form-group row">
 				<div class="col-1">
				</div>
				<div class="col-5">
					<a href="UserListServlet" type="submit" class="btn btn-ligth btn-block">キャンセル</a>
				</div>
				<div class="col-5">
					<form action="UserDeleteServlet" method ="post">
						<input type="hidden" name="id" value="${user.id}">
							<button type="submit" class="btn btn-primary btn-block">OK</button>
					</form>
				</div>
			</div>

	</div>

</body>
</html>