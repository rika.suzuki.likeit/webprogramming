<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html lang="ja">
<head>
	<meta charset="UTF-8">
	<title>ログイン画面</title>
	<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">

</head>
<body>
	<div class="container">

		<br>

		<!-- エラーメッセージがある場合は表示 -->
		<c:if test= "${errMsg != null}">
			<div class="alert alert-danger" role="alert">
				${errMsg}
			</div>
		</c:if>

		<h1 class="text-center">ログイン画面</h1>

		<form action="LoginServlet" method="post">
			<div class="form-group row">
				<label class="col-sm-3 col-form-label">ログインID</label>
				<div class="col-sm-9">
					<input type="text" name="loginId" class="form-control">
				</div>
			</div>

			<div class="form-group row">
				<label class="col-sm-3 col-form-label">パスワード</label>
				<div class="col-sm-9">
					<input type="password" name="password" class="form-control">
				</div>
			</div>

			<div class="text-center">
				<button type="submit" class="btn btn-lg btn-primary ">ログイン</button>
			</div>
		</form>

	</div>
</body>
</html>