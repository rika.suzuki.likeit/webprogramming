<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>


<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="UTF-8">
<title>ユーザ情報更新</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
</head>
<body>
	<header>
		<nav class="navbar navbar-expand navbar-secondary bg-secondary">
		<div class="collapse navbar-collapse justify-content-end">
			<ul class="navbar-nav">
				<li class="nav-text">${userInfo.name}さん</li>
				<li class="nav-item">
					<a class="nav-link" href="LogoutServlet">ログアウト</a>
				</li>
			</ul>
		</div>
		</nav>
	</header>
	<br>
	<div class="container">

		<!-- エラーメッセージがある場合は表示 -->
		<c:if test= "${errMsg != null}">
			<div class="alert alert-danger" role="alert">
				${errMsg}
			</div>
		</c:if>

		<h1 class="text-center">ユーザ情報更新</h1>

		<form action="UserUpdateServlet" method="post" class="form-horizontal">
			<input type="hidden" name="id" value="${user.id}">
			<input type="hidden" name="login_id" value="${user.login_id}">

			<div class="form-goup row">
				<label class="col-sm-2 col-form-label">ログインID</label>
				<div class="col-sm-10">
					<p class="form-control-plaintext">${user.login_id}</p>
				</div>
			</div>

			<div class="form-group row">
				<label class="col-sm-2 col-form-label">パスワード</label>
				<div class="col-sm-10">
					<input type="password" class="form-control" name="password">
				</div>
			</div>

			<div class="form-group row">
				<label class="col-sm-2 col-form-label">パスワード(確認)</label>
				<div class="col-sm-10">
					<input type="password" class="form-control" name="password2">
				</div>
			</div>

			<div class="form-group row">
				<label class="col-sm-2 col-form-label">ユーザ名</label>
				<div class="col-sm-10">
					<input type="text" class="form-control" name="name" value="${user.name}">
				</div>
			</div>

			<div class="form-group row">
				<label class="col-sm-2 col-form-label">生年月日</label>
				<div class="col-sm-10">
					<input type="date" class="form-control" name="birthDate" value="${user.birthDateStr}">
				</div>
			</div>

			<div class="text-center">
				<button type="submit" class="btn btn-primary">更新</button>
			</div>

			<a href="UserListServlet">戻る</a>
		</form>
	</div>
</body>
</html>