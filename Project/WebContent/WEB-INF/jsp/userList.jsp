<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html lang="ja">
<head>
	<meta charset="UTF-8">
	<title>ユーザー一覧</title>
	<link rel="stylesheet"
		href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
		integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
		crossorigin="anonymous">
</head>
<body>

	<header>
		<nav class="navbar navbar-expand navbar-secondary bg-secondary">
		<div class="collapse navbar-collapse justify-content-end">
			<ul class="navbar-nav">
				<li class="nav-text">${userInfo.name}さん</li>
				<li class="nav-item">
					<a class="nav-link" href="LogoutServlet">ログアウト</a>
				</li>
			</ul>
		</div>
		</nav>
	</header>

	<br>

	<div class="container">
		<h1 class="text-center">ユーザー一覧</h1>

		<div class="text-right">
			<a href="UserRegisterServlet">新規登録</a>
		</div>

		<br>

		<form action="UserListServlet" method="post">

			<div class="form-group row">
				<label class="col-sm-3 col-form-label">ログインID</label>
				<div class="col-sm-9">
					<input type="text" class="form-control" name="loginId">
				</div>
			</div>

			<div class="form-group row">
				<label class="col-sm-3 col-form-label">ユーザー名</label>
				<div class="col-sm-9">
					<input type="text" class="form-control" name="name">
				</div>
			</div>

			<div class="form-group row">

				<label class="col-sm-3 col-form-label">生年月日</label>

				<div class="col-sm-9 row">
					<div class="col-sm-5">
						<input type="date" class="form-control" name="birthDate1">
					</div>
					<div class="col-sm-1 text-center col-form-label">
					～
					</div>
					<div class="col-sm-5">
						<input type="date" class="form-control" name="birthDate2">
					</div>
				</div>

			</div>

			<div class="text-right">
				<button type="submit" class="btn btn-primary">検索</button>
			</div>
		</form>

		<br>

		<div class="table-responsive">
			<table class="table table-bordered">
				<thead class="thead-light">
					<tr>
						<th>ログインID</th>
						<th>ユーザ名</th>
						<th>生年月日</th>
						<th></th>
					</tr>
				</thead>

					<c:forEach var="user" items="${userList}">
						<tr>
							<td>${user.login_id}</td>
							<td>${user.name}</td>
							<td>${user.birthDate}</td>

							<td>
								<a class="btn btn-primary" href="UserDetailServlet?id=${user.id}">詳細</a>

								<!-- ログインが管理者の場合はボタン全表示、管理者以外の場合は詳細のみ表示(本人のボタンは全表示) -->
								<c:if test="${userInfo.id==1}">
									<a class="btn btn-success" href="UserUpdateServlet?id=${user.id}">更新</a>
									<a class="btn btn-danger" href="UserDeleteServlet?id=${user.id}">削除</a>
								</c:if>

								<c:if test="${userInfo.id!=1}">
									<c:if test="${userInfo.id==user.id}">
										<a class="btn btn-success" href="UserUpdateServlet?id=${user.id}">更新</a>
										<a class="btn btn-danger" href="UserDeleteServlet?id=${user.id}">削除</a>
									</c:if>
								</c:if>
							</td>
						</tr>
					</c:forEach>
			</table>
		</div>
	</div>
</body>
</html>