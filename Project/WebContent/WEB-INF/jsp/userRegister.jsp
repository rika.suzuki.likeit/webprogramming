<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html lang="jp">
<head>
	<meta charset="UTF-8">
	<title>ユーザー新規登録</title>
	<link rel="stylesheet"
		href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
		integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
		crossorigin="anonymous">
</head>
<body>
	<header>
		<nav class="navbar navbar-expand navbar-secondary bg-secondary">
		<div class="collapse navbar-collapse justify-content-end">
			<ul class="navbar-nav">
				<li class="nav-text">${userInfo.name}さん</li>
				<li class="nav-item">
					<a class="nav-link" href="LogoutServlet">ログアウト</a>
				</li>
			</ul>
		</div>
		</nav>
	</header>
	<br>

	<div class="container">

		<!-- エラーメッセージがある場合は表示 -->
		<c:if test= "${errMsg != null}">
			<div class="alert alert-danger" role="alert">
				${errMsg}
			</div>
		</c:if>

		<h1 class="text-center">ユーザー新規登録</h1>
		<form action="UserRegisterServlet" method="post" class="form-horizontal">
			<div class="form-group row">
				<label class="col-sm-2 col-form-label">ログインID</label>
				<div class="col-sm-10">
					<input type="text" name="loginId" class="form-control" value=${loginId}>
				</div>
			</div>

			<div class="form-group row">
				<label class="col-sm-2 col-form-label">パスワード</label>
				<div class="col-sm-10">
					<input type="password" name="password" class="form-control">
				</div>
			</div>

			<div class="form-group row">
				<label class="col-sm-2 col-form-label">パスワード(確認)</label>
				<div class="col-sm-10">
					<input type="password" name="password2" class="form-control">
				</div>
			</div>

			<div class="form-group row">
				<label class="col-sm-2 col-form-label">ユーザー名</label>
				<div class="col-sm-10">
					<input type="text" name="name" class="form-control" value=${name}>
				</div>
			</div>

			<div class="form-group row">
				<label class="col-sm-2 col-form-label">生年月日</label>
				<div class="col-sm-10">
					<input type="date" name="birthDate" class="form-control" value=${birthDate}>
				</div>
			</div>

			<div class="text-center">
				<button type="submit" class="btn btn-primary">登録</button>
			</div>

			<a href="UserListServlet">戻る</a>
		</form>


	</div>



</body>
</html>